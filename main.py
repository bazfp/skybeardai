import os
import re
import subprocess
import tempfile
import logging
import telegram
import multiprocessing
import pexpect
import sys
from telegram import Update
from telegram.ext import filters, MessageHandler, ApplicationBuilder, CommandHandler, ContextTypes

#configure logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
CHAT_CONTEXTS = dict()


def strip_ansi_codes(s):
    """
    >>> import blessings
    >>> term = blessings.Terminal()
    >>> foo = 'hidden'+term.clear_bol+'foo'+term.color(5)+'bar'+term.color(255)+'baz'
    >>> repr(strip_ansi_codes(foo))
    u'hiddenfoobarbaz'
    """
    return re.sub(r'\x1b\[([0-9,A-Z]{1,2}(;[0-9]{1,2})?(;[0-9]{3})?)?[m|K]?', '', s)

def setup_chatbot():
    model = pexpect.spawnu('./chat -m ggml-alpaca-7b-q4.bin -t {} --top_p 0 '.format( multiprocessing.cpu_count()), encoding='utf-8', echo=False)
    #model.logfile = sys.stdout

    try:
      model.expect(".*If you want to submit another line, end your input in .*")
      logging.info("Match new startup" + model.before)
      model.expect('>')
      logging.info("Ready for prompt");
    except:
      logging.info("Error starting chat")
      
    return model
 

def run_chatbot(text):
    temp = tempfile.NamedTemporaryFile()
    try:
        temp.write(bytes(text, 'utf-8'))
    finally:
        temp.close()

    result = model
    try:
      result.sendline(text.replace('"', ''))
      result.expect('>', timeout=90)
      logging.info("Match result!" + result.before + " after " + result.after )

    except:
        logging.info("Error Expecting Result from Chat")
        logging.info(result.before)

    outputAnswer = strip_ansi_codes(result.before)

#    result = subprocess.run(['./chat', '-p', text.replace('"', ''), '-m', 'ggml-alpaca-13b-q4.bin', '-t', multiprocessing.cpu_count()  ], stdout=subprocess.PIPE)

    logging.info(outputAnswer)
    return outputAnswer

def template_input(context, update, input_text):
    chat = update.message.chat
    bot = context.bot.bot
    user = update.message.from_user

    template = []
    template.append(CHAT_CONTEXTS['base'].format(bot.first_name, user.first_name))
    if chat.type != telegram.constants.ChatType.PRIVATE:
        template.append(CHAT_CONTEXTS['groupchat'].format(chat.title))
    template.append(CHAT_CONTEXTS['custom'])
    template.append(input_text)
    return ' '.join(template)
    

# ask command handler
async def ask(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=telegram.constants.ChatAction.TYPING)
    try:
        input_text = ' '.join(context.args)
        full_prompt = template_input(context, update, input_text)
        logging.info('PROMPT:', full_prompt)
        result = run_chatbot(full_prompt)
    except Exception as e:
        logging.info(e)
        await update.message.reply_text("oops something went wrong", quote=True)

    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    await update.message.reply_text(result, quote=quote)
     

if __name__ == '__main__':
    

    model = setup_chatbot()
    for f in os.listdir('contexts'):
        filename = os.fsdecode(f)
        if filename.endswith(".txt"):
            context_name = os.path.splitext(os.path.basename(filename))[0]
            with open(os.path.join('contexts', filename)) as context_file: 
                CHAT_CONTEXTS[context_name] = context_file.read().replace('\n', ' ')

    application = ApplicationBuilder().token(os.environ['SKYBEARD_AI_TOKEN']).build()
    
    ask_handler = CommandHandler('ask', ask)
    application.add_handler(ask_handler)
    
    application.run_polling()
